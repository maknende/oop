<?php

    class Animal
    {
        public $name;
        public $kind;
        public $legs = 4;
        public $cold_blooded = "No";

        public function __construct($string, $type)
        {
            $this->name = $string;
            $this->kind = $type;
        }

        public function get_name()
        {
            echo "Animal name: " . $this->name . "<br>";
        }

        public function get_kind()
        {
            echo "Animal type: " . $this->kind . "<br>";
        }
        
        public function get_legs()
        {
            echo "Total legs: " . $this->legs . "<br>";
        }

        public function get_cold_blooded()
        {
            echo "Cold Blooded? " . $this->cold_blooded . "<br>";
        }
    }